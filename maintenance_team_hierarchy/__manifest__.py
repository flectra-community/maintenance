# Copyright 2019 Creu Blanca
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Maintenance Team Hierarchy",
    "summary": """
        Create hierarchies on teams""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "Creu Blanca,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/maintenance",
    "depends": ["maintenance"],
    "data": ["views/maintenance_team.xml"],
    "demo": ["demo/maintenance_team_demo.xml", "demo/maintenance_request_demo.xml"],
}
