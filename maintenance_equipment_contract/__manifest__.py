# Copyright 2019 Creu Blanca
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Maintenance Equipment Contract",
    "summary": """
        Manage equipment contracts""",
    "version": "2.0.1.0.1",
    "license": "AGPL-3",
    "author": "Creu Blanca,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/maintenance",
    "depends": ["contract", "base_maintenance"],
    "data": [
        "security/ir.model.access.csv",
        "security/contract_security.xml",
        "views/contract_contract.xml",
        "views/maintenance_equipment.xml",
    ],
    "demo": ["demo/maintenance_equipment_contract_demo.xml"],
}
