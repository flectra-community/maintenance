# Flectra Community / maintenance

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[maintenance_project_plan](maintenance_project_plan/) | 2.0.1.0.0| Adds project and task to a Maintenance Plan
[maintenance_project](maintenance_project/) | 2.0.1.1.0| Adds projects to maintenance equipments and requests
[maintenance_product](maintenance_product/) | 2.0.1.0.0| Maintenance Product
[base_maintenance_config](base_maintenance_config/) | 2.0.1.0.0| Provides general settings for the Maintenance App
[base_maintenance](base_maintenance/) | 2.0.1.1.0| Base Maintenance
[maintenance_equipment_tags](maintenance_equipment_tags/) | 2.0.1.0.0|         Adds category tags to equipment
[maintenance_plan](maintenance_plan/) | 2.0.1.1.0| Extends preventive maintenance planning
[maintenance_equipment_scrap](maintenance_equipment_scrap/) | 2.0.1.0.0| Enhance the functionality for Scrapping Equipments
[maintenance_remote](maintenance_remote/) | 2.0.1.0.0|         Define remote on maintenance request
[maintenance_timesheet](maintenance_timesheet/) | 2.0.1.0.0| Adds timesheets to maintenance requests
[maintenance_team_hierarchy](maintenance_team_hierarchy/) | 2.0.1.0.0|         Create hierarchies on teams
[maintenance_request_stage_transition](maintenance_request_stage_transition/) | 2.0.1.0.0|         Manage transition visibility and management between stages
[base_maintenance_group](base_maintenance_group/) | 2.0.1.0.1| Provides base access groups for the Maintenance App
[maintenance_equipment_contract](maintenance_equipment_contract/) | 2.0.1.0.1|         Manage equipment contracts
[maintenance_equipment_sequence](maintenance_equipment_sequence/) | 2.0.1.0.0|         Adds sequence to maintenance equipment defined in the equipment's        category
[maintenance_account](maintenance_account/) | 2.0.1.0.0| Maintenance Account
[maintenance_plan_activity](maintenance_plan_activity/) | 2.0.1.0.1|         This module allows defining in the maintenance plan activities that        will be created once the maintenance requests are created as a        consequence of the plan itself.
[maintenance_request_sequence](maintenance_request_sequence/) | 2.0.1.0.0|         Adds sequence to maintenance requests
[maintenance_equipment_status](maintenance_equipment_status/) | 2.0.1.0.0| Maintenance Equipment Status
[maintenance_equipment_hierarchy](maintenance_equipment_hierarchy/) | 2.0.1.0.1| Manage equipment hierarchy
[maintenance_equipment_image](maintenance_equipment_image/) | 2.0.1.0.0| Adds images to equipment.


