# Copyright 2020 ForgeFlow S.L. (https://forgeflow.com)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Maintenance Equipment Sequence",
    "summary": """
        Adds sequence to maintenance equipment defined in the equipment's
        category""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "ForgeFlow S.L.," "Odoo Community Association (OCA)",
    "maintainers": ["AdriaGForgeFlow"],
    "website": "https://gitlab.com/flectra-community/maintenance",
    "depends": ["maintenance"],
    "data": ["views/maintenance_views.xml"],
}
